/// <reference types="cypress" />

describe('Logout', () => {
  beforeEach(() => cy.login());
  it('successfullt', () => {
    cy.logout()

    cy.url().should('be.equal', `${Cypress.config('baseUrl')}users/sign_in`)
  });
});